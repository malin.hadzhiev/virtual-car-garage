package com.telerikproject.virtualcargarage.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class EmailConfig {

    public static final String EMAIL_REGISTER_MESSAGE = "Thank you for using my Car Service! You can find your credentials below:\n" +
            "Username: %s\n" +
            "Password: %s\n";

    @Bean
    public SimpleMailMessage templateSimpleMessage() {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setText(EMAIL_REGISTER_MESSAGE);

        return message;
    }

    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("hadzhiev.service@gmail.com");
        mailSender.setPassword("Telerik2019");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }

}

