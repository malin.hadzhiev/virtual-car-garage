package com.telerikproject.virtualcargarage.controllers;

import com.telerikproject.virtualcargarage.entities.Car;
import com.telerikproject.virtualcargarage.entities.Customer;
import com.telerikproject.virtualcargarage.models.CreateVisitModel;
import com.telerikproject.virtualcargarage.services.contracts.CarService;
import com.telerikproject.virtualcargarage.services.contracts.CarServiceService;
import com.telerikproject.virtualcargarage.services.contracts.CustomerService;
import com.telerikproject.virtualcargarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private CustomerService customerService;
    private CarService carService;
    private VisitService visitService;
    private CarServiceService carServiceService;

    @Autowired
    public AdminController(CustomerService customerService, CarService carService, VisitService visitService, CarServiceService carServiceService) {
        this.customerService = customerService;
        this.carService = carService;
        this.visitService = visitService;
        this.carServiceService = carServiceService;
    }

    @GetMapping
    public String getAllTables(Model model) {
        model.addAttribute("customers", customerService.getAllCustomers());
        model.addAttribute("cars", carService.getAllCars());
        model.addAttribute("visits", visitService.getAllVisits());
        model.addAttribute("services", carServiceService.getAllCarServices());
        return "admin";
    }

    @GetMapping("/customer/edit/{id}")
    public String getEditCustomerView(Model model, @PathVariable("id") long id) {
        Customer customer = customerService.getCustomerById(id);
        model.addAttribute("customer", customer);

        return "edit-customer";
    }

    @PutMapping("/customer/edit/{id}")
    public String editCustomer(@ModelAttribute Customer customer,
                           @PathVariable long id) {

        customerService.editCustomer(id, customer);

        return "redirect:/admin";
    }

    @GetMapping("/customer/delete/{id}")
    public String deleteCustomer(@PathVariable long id) {
        customerService.deleteCustomer(id);
        return "redirect:/admin";
    }

    @GetMapping("/new-car")
    public String getCreateCarView(Model model){
        model.addAttribute("car", new Car());
        model.addAttribute("cars", carService.getAllCars());
        model.addAttribute("customers", customerService.getAllCustomers());

        return "add-car";
    }

    @PostMapping("/new-car")
    public String createCar(@Valid @ModelAttribute Car car, Model model) {
        String errorMessage = validateCar(car);

        if (!errorMessage.isEmpty()){
            model.addAttribute("error", errorMessage);
            model.addAttribute("car", new Car());
            model.addAttribute("cars", carService.getAllCars());
            model.addAttribute("customers", customerService.getAllCustomers());

            return "add-car";
        }

        carService.createCar(car);

        return "redirect:/admin";
    }

    @GetMapping("/car/edit/{id}")
    public String getEditCarView(Model model, @PathVariable("id") int id) {
        Car car = carService.getCarById(id);
        model.addAttribute("car", car);
        model.addAttribute("customers", customerService.getAllCustomers());

        return "edit-car";
    }

    @PutMapping("/car/edit/{id}")
    public String editCar(@ModelAttribute Car car, @PathVariable int id) {
        carService.editCar(id, car);
        return "redirect:/admin";
    }

    @GetMapping("/car/delete/{id}")
    public String deleteCar(@PathVariable long id) {
        carService.deleteCar(id);
        return "redirect:/admin";
    }

           @GetMapping("/new-visit")
        public String getCreateVisitView(Model model){
            model.addAttribute("visit", new CreateVisitModel());
            model.addAttribute("visits", visitService.getAllVisits());
            model.addAttribute("cars", carService.getAllCars());
            model.addAttribute("services", carServiceService.getAllCarServices());
            model.addAttribute("service", new com.telerikproject.virtualcargarage.entities.CarService());

            return "add-visit";
        }

        @PostMapping("/new-visit")
        public String createVisit(@Valid @ModelAttribute CreateVisitModel createVisitModel,
                                  BindingResult bindingResult) {

//            if (bindingResult.hasErrors()) {
//                return "add-visit";
//            }

            visitService.createVisit(createVisitModel.getRegistration(), createVisitModel.getService());

        return "redirect:/admin";
    }

    @GetMapping("/visit/edit/{id}")
    public String getEditVisitView(Model model, @PathVariable("id") int id) {
        model.addAttribute("visit", new CreateVisitModel());
        model.addAttribute("cars", carService.getAllCars());
        model.addAttribute("services", carServiceService.getAllCarServices());

        return "edit-visit";
    }

    @PutMapping("/visit/edit/{id}")
    public String editVisit(@ModelAttribute CreateVisitModel createVisitModel, @PathVariable int id) {
        visitService.editVisit(id, createVisitModel);
        return "redirect:/admin";
    }

    @GetMapping("/visit/delete/{id}")
    public String deleteVisit(@PathVariable long id) {
        visitService.deleteVisit(id);
        return "redirect:/admin";
    }

    @GetMapping("/new-service")
    public String createServiceForm(Model model){
        model.addAttribute("service", new com.telerikproject.virtualcargarage.entities.CarService());
        model.addAttribute("services", carServiceService.getAllCarServices());

        return "add-service";
    }

    @PostMapping("/new-service")
    public String createVisit(@Valid @ModelAttribute com.telerikproject.virtualcargarage.entities.CarService carService, BindingResult bindingErrors, Model model) {
        model.addAttribute("service", new com.telerikproject.virtualcargarage.entities.CarService());
        model.addAttribute("services", carServiceService.getAllCarServices());

        carServiceService.createCarService(carService);

        return "redirect:/admin";
    }

    @GetMapping("/service/edit/{id}")
    public String getEditServiceView(Model model, @PathVariable("id") long id) {
        com.telerikproject.virtualcargarage.entities.CarService carService = carServiceService.getCarServiceById(id);
        model.addAttribute("service", carService);

        return "edit-service";
    }

    @PutMapping("/service/edit/{id}")
    public String editService(@ModelAttribute com.telerikproject.virtualcargarage.entities.CarService carService,
                               @PathVariable long id) {

        carServiceService.editCarService(id, carService);

        return "redirect:/admin";
    }

    @GetMapping("/service/delete/{id}")
    public String deleteService(@PathVariable long id) {
        carServiceService.deleteService(id);
        return "redirect:/admin";
    }

    private String validateCar(Car car) {
       List<Car> cars = carService.getAllCars();
       List<String> carVins = new ArrayList<>();
       List<String> registrations = new ArrayList<>();

        for (Car car1: cars ) {
            carVins.add(car1.getCarVin());
            registrations.add(car1.getRegistration());
        }

        if (carVins.contains(car.getCarVin())){
            return "Car with this VIN already exist!";
        }

        if (registrations.contains(car.getRegistration())){
            return "Car with this registration already exist!";
        }

        return "";
    }
}