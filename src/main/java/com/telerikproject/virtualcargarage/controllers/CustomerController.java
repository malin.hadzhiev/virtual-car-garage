package com.telerikproject.virtualcargarage.controllers;

import com.telerikproject.virtualcargarage.entities.Customer;
import com.telerikproject.virtualcargarage.entities.User;
import com.telerikproject.virtualcargarage.exceptions.WrongPasswordException;
import com.telerikproject.virtualcargarage.services.contracts.UserService;
import com.telerikproject.virtualcargarage.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;

@Controller
@RequestMapping("/customers")
public class CustomerController {

    private CustomerService customerService;
    private UserService userService;

    private static String uploadDirectory = System.getProperty("user.dir") + "/src/main/resources/static/images";

    @Autowired
    public CustomerController(CustomerService customerService, UserService userService) {
        this.customerService = customerService;
        this.userService = userService;
    }

    @RequestMapping("/profile")
    public String UploadPage() {
        return "profile";
    }

    @PostMapping("/upload")
    public String upload(@RequestParam("files") MultipartFile[] files, Principal principal) {
        StringBuilder fileNames = new StringBuilder();
        for (MultipartFile file : files) {
            Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
            fileNames.append(file.getOriginalFilename());
            try {
                Files.write(fileNameAndPath, file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Customer customer = customerService.getCustomerByEmail(principal.getName());

        customer.setImagePath(fileNames.toString());

        customerService.save(customer);

        return "redirect:/profile";
    }

    @PostMapping("/updatePassword")
    public String changeUserPassword(@RequestParam("newPassword") String newPassword,
                                     @RequestParam("currentPassword") String currentPassword,
                                     RedirectAttributes redirectAttributes
                                     ) {

        User user = userService.getUserByEmail(
                SecurityContextHolder.getContext().getAuthentication().getName());

        try {
            userService.changePassword(user, currentPassword, newPassword);
        } catch (WrongPasswordException e){
            redirectAttributes.addFlashAttribute("error", e.getMessage());
            return "redirect:/profile";
        }
        return "redirect:/logout";
    }
}

