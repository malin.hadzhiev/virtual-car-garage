package com.telerikproject.virtualcargarage.controllers;

import com.telerikproject.virtualcargarage.entities.Customer;
import com.telerikproject.virtualcargarage.services.contracts.CarService;
import com.telerikproject.virtualcargarage.services.contracts.CustomerService;
import com.telerikproject.virtualcargarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class LoginController {

    private CustomerService customerService;
    private CarService carService;
    private VisitService visitService;

    @Autowired
    public LoginController(CustomerService customerService, CarService carService, VisitService visitService) {
        this.customerService = customerService;
        this.carService = carService;
        this.visitService = visitService;
    }

    @GetMapping("/login")
    public String showLogin() {
        return "login";
    }

    @GetMapping("/access-denied")
    public String showAccessDenied() {
        return "access-denied";
    }

    @GetMapping("/profile")
    public String showProfile(Model model, Authentication principal) {
        Customer currentCustomer = customerService.getCustomerByEmail(principal.getName());

        model.addAttribute("customer", currentCustomer);
        model.addAttribute("myCars", carService.getAllCarsByCustomerEmail(principal.getName()));
        model.addAttribute("myVisits", visitService.getAllVisitsByCustomerEmail(principal.getName()));

        return "profile";
    }
}

