package com.telerikproject.virtualcargarage.controllers;

import com.telerikproject.virtualcargarage.models.CreateCustomerModel;
import com.telerikproject.virtualcargarage.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class RegistrationController {

    private CustomerService customerService;
    private UserDetailsManager userDetailsManager;

    @Autowired
    RegistrationController(CustomerService customerService, UserDetailsManager userDetailsManager) {
        this.customerService = customerService;
        this.userDetailsManager = userDetailsManager;
    }

    @GetMapping("/contact")
    public String showContactForm(Model model) {
        model.addAttribute("createCustomerModel", new CreateCustomerModel());
        return "contact";
    }

    @PostMapping("/contact")
    public String createNewCustomer(@ModelAttribute CreateCustomerModel createCustomerModel, Model model) {
        if(userDetailsManager.userExists(createCustomerModel.getUsername())) {
            model.addAttribute("error", "Customer with this email already exists!");
            return "contact";
        }
        customerService.createCustomer(createCustomerModel);
        return "redirect:/success-registration";
    }

    @GetMapping("/success-registration")
    public String showRegisterConfirmation(Model model) {
        model.addAttribute("createCustomerModel", new CreateCustomerModel());
        return "success-registration";
    }
    @RequestMapping(value="/confirm-account", method= {RequestMethod.GET, RequestMethod.POST})
    public String confirmUserAccount(@RequestParam("token") String confirmationToken)
    {
        customerService.confirmAccount(confirmationToken);
        return "accountVerified";
    }
}
