package com.telerikproject.virtualcargarage.controllers.rest;

import com.telerikproject.virtualcargarage.entities.Car;
import com.telerikproject.virtualcargarage.entities.Customer;
import com.telerikproject.virtualcargarage.exceptions.CustomerDoesNotExistException;
import com.telerikproject.virtualcargarage.services.contracts.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/cars")
public class CarRestController {

    private CarService carService;

    @Autowired
    public CarRestController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping("/all")
    public List<Car> getAllCars() {
        List<Car> cars = carService.getAllCars();
        try {
            return cars;
        } catch (CustomerDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    public Car getCarById(@PathVariable long id) {
        try {
            return carService.getCarById(id);
        } catch (CustomerDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/all/by-customer-email/{email}")
    public List<Car> getAllCarsByCustomerEmail(@PathVariable String email) {
        List<Car> cars = carService.getAllCarsByCustomerEmail(email);
        try {
            return cars;
        } catch (CustomerDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


}
