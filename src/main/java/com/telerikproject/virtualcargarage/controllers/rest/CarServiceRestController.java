package com.telerikproject.virtualcargarage.controllers.rest;

import com.telerikproject.virtualcargarage.entities.CarService;
import com.telerikproject.virtualcargarage.exceptions.CustomerDoesNotExistException;
import com.telerikproject.virtualcargarage.services.contracts.CarServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/car-services")
public class CarServiceRestController {

    private CarServiceService carServiceService;

    @Autowired
    public CarServiceRestController(CarServiceService carServiceService) {
        this.carServiceService = carServiceService;
    }

    @GetMapping("/all")
    public List<CarService> getAllCarServices() {
        List<CarService> carServices = carServiceService.getAllCarServices();
        try {
            return carServices;
        } catch (CustomerDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    public CarService getCarServiceById(@PathVariable long id) {
        try {
            return carServiceService.getCarServiceById(id);
        } catch (CustomerDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/new-service")
    public void createCarService(@RequestBody CarService carService){
        carServiceService.createCarService(carService);
    }

    @PutMapping("/edit-service/{id}")
    public void editAutoService(@PathVariable long id, @RequestBody CarService carService) {
        carServiceService.editCarService(id, carService);
    }

    @DeleteMapping("/{id}")
    public void deleteAutoService(@PathVariable long id) {
        carServiceService.deleteService(id);
    }
}