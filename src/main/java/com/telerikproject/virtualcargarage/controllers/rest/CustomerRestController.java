package com.telerikproject.virtualcargarage.controllers.rest;

import com.telerikproject.virtualcargarage.entities.Customer;
import com.telerikproject.virtualcargarage.exceptions.CustomerDoesNotExistException;
import com.telerikproject.virtualcargarage.models.CreateCustomerModel;
import com.telerikproject.virtualcargarage.models.CustomerModel;
import com.telerikproject.virtualcargarage.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/customers")
public class CustomerRestController {

    private CustomerService customerService;

    @Autowired
    public CustomerRestController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/all")
    public List<Customer> getAllCustomers(){
        try {
            return customerService.getAllCustomers();
        } catch (CustomerDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    public Customer getCustomerById(@PathVariable long id) {
        try {
            return customerService.getCustomerById(id);
        } catch (CustomerDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/logged")
    public Customer getLoggedCustomer(Principal principal) {
        Customer customer = customerService.getCustomerByEmail(principal.getName());

        return customer;
    }

    @PostMapping("/new")
    public void createCustomer(@RequestBody @Valid CreateCustomerModel createCustomerModel) {
        customerService.createCustomer(createCustomerModel);
    }

    @PutMapping("/edit")
    public CustomerModel editUser(@RequestBody @Valid CustomerModel customerModel) {
        return  customerService.editCustomer(customerModel);
    }
//
//    @GetMapping("/page/")
//    public Page<Customer> getPage(Pageable pageable, Principal principal) {
//        return customerService.customerPagination(pageable, principal.getName());
//    }
}
