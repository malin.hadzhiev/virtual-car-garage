package com.telerikproject.virtualcargarage.controllers.rest;

import com.telerikproject.virtualcargarage.entities.Visit;
import com.telerikproject.virtualcargarage.exceptions.CustomerDoesNotExistException;
import com.telerikproject.virtualcargarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/visits")
public class VisitRestController {

    private VisitService visitService;

    @Autowired
    public VisitRestController (VisitService visitService){
        this.visitService = visitService;
    }

    @GetMapping("/all")
    public List<Visit> getAllVisits() {
        List<Visit> visits = visitService.getAllVisits();
        try {
            return visits;
        } catch (CustomerDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    public Visit getVisitById(@PathVariable long id) {
        try {
            return visitService.getVisitById(id);
        } catch (CustomerDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void deleteVisit(@PathVariable long id) {
        visitService.deleteVisit(id);
    }
}
