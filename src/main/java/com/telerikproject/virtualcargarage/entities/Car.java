package com.telerikproject.virtualcargarage.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @Column(name = "license_plate")
    @NotNull(message = "Registration is required.")
    @Size(min = 6,
            max = 10,
            message = "The car registration length must be min {min} characters and max {max} characters!")
    @Pattern(regexp = "[A-Z0-9\u0400-\u04FF]*",
            message = "Car registration number must contains large letters and numbers!")
    private String registration;

    @Column(name = "car_vin")
    @NotNull(message = "Car VIN is required.")
    @Size(min = 17,max = 17)
    @Pattern(regexp = "[A-Z0-9]*",
            message = "Car VIN number must contains seventeen characters of large letters and numbers!")
    private String carVin;

    @Column(name = "manufacturer")
    @Size(
            min = 2,
            max = 30,
            message = "The manufacturer name length must be min {min} characters and max {max} characters!")
    private String manufacturer;

    @Column(name = "model")
    @Size(
            min = 2,
            max = 30,
            message = "The model name length must be min {min} characters and max {max} characters!")
    private String model;

    @Column(name = "year")
    private int year;

    @JsonIgnore
    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Visit> visits;

    @Override
    public String toString() {
        return  registration;
    }
}
