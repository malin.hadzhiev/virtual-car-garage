package com.telerikproject.virtualcargarage.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "services")
public class CarService {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "service_type")
    @Size(
            min = 2,
            max = 30,
            message = "The service type length must be min {min} characters and max {max} characters!")
    private String serviceType;

    @Column(name = "price")
    @Positive(message = "Price should be a positive number.")
    private double price;

    @JsonIgnore
    @ManyToMany(mappedBy = "carServices", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Visit> carVisits;

    @Override
    public String toString() {
        return  serviceType;
    }
}
