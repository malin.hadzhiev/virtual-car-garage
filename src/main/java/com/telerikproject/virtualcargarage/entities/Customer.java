package com.telerikproject.virtualcargarage.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "customers")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "email")
    @Email(message = "Email should be valid.")
    @NotNull(message = "Email is required.")
    private String email;

    @Column(name = "name")
    @Pattern(regexp = "[A-Za-z ]*",
            message = "The customer name should contain only English letters!")
    @Size(
            min = 2,
            max = 40,
            message = "The customer name length must be min {min} characters and max {max} characters!")
    private String name;

    @Column(name = "phone")
    @Pattern(regexp = "[0-9+]*",
            message = "The customer phone number should't contain any letters!")
    @Size(
            min = 5,
            max = 20,
            message = "The customer phone number length must be min {min} characters and max {max} characters!")
    private String phone;

    @Column(name = "image_path")
    private String imagePath;

    @JsonIgnore
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Car> cars;
}
