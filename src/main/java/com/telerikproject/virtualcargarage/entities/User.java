package com.telerikproject.virtualcargarage.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User {
    @Id
    @Column(name = "username")
    @Email(message = "Email should be valid.")
    @NotNull(message = "Email is required.")
    private String username;

    @Size(min = 5, message = "Password must be over {min} characters")
    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private boolean enabled;

}
