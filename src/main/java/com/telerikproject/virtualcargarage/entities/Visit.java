package com.telerikproject.virtualcargarage.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "visits")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;

    @Column(name = "date")
    private LocalDateTime date;

    @Column(name = "total_price")
    private double totalPrice;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "visit_services",
            joinColumns = @JoinColumn(name = "visit_id") ,
            inverseJoinColumns = @JoinColumn(name = "service_id")
    )
    private Set<CarService> carServices;

    @Override
    public String toString() {
        return  car.getRegistration();
    }
}
