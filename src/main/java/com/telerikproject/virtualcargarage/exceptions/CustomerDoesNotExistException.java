package com.telerikproject.virtualcargarage.exceptions;

public class CustomerDoesNotExistException extends RuntimeException {
    public CustomerDoesNotExistException() {
        super();
    }
}
