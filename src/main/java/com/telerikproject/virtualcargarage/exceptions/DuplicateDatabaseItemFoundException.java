package com.telerikproject.virtualcargarage.exceptions;

import org.springframework.dao.DuplicateKeyException;

public class DuplicateDatabaseItemFoundException extends DuplicateKeyException {
    public DuplicateDatabaseItemFoundException(String message) {

        super(String.format("%s", message));
    }
}

