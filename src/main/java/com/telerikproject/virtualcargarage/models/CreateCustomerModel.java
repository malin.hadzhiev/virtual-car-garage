package com.telerikproject.virtualcargarage.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CreateCustomerModel {

    @NotNull
    @Column(name = "email")
    @Email(message = "Email should be valid.")
    @NotNull(message = "Email is required.")
    private String username;

    @NotNull
    @Column(name = "name")
    @Pattern(regexp = "[A-Za-z ]*",
            message = "The customer name should contain only English letters!")
    @Size(
            min = 2,
            max = 40,
            message = "The customer name length must be min {min} characters and max {max} characters!")
    private String name;

    @NotNull
    @Column(name = "phone")
    @Pattern(regexp = "[0-9+]*",
            message = "The customer phone number should't contain any letters!")
    @Size(
            min = 5,
            max = 20,
            message = "The customer phone number length must be min {min} characters and max {max} characters!")
    private String phone;
}
