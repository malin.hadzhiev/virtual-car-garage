package com.telerikproject.virtualcargarage.models;

import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateVisitModel {

    private String registration;

    private List<String> service;
}
