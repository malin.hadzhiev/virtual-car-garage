package com.telerikproject.virtualcargarage.models;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerModel {

    @NotNull
    @PositiveOrZero
    private long id;

    private String name;

    private String phone;
}