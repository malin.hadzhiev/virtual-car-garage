package com.telerikproject.virtualcargarage.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
public class EditCustomerModel {

    private long id;

    private String email;

    @Pattern(regexp = "[A-Za-z ]*",
            message = "The customer name should contain only English letters!")
    @Size(
            min = 2,
            max = 40,
            message = "The customer name length must be min {min} characters and max {max} characters!")
    private String name;

    @Pattern(regexp = "[0-9+]*",
            message = "The customer phone number should't contain any letters!")
    @Size(
            min = 5,
            max = 20,
            message = "The customer phone number length must be min {min} characters and max {max} characters!")
    private String phone;

    private String currentPassword;

    @Size(
            min = 3,
            max = 20,
            message = "The user password length must be min {min} characters and max {max} characters!"
    )
    private String newPassword;

//    @Size(
//            min = 3,
//            max = 20,
//            message = "The user password length must be min {min} characters and max {max} characters!"
//    )
//    private String confirmationPassword;
}
