package com.telerikproject.virtualcargarage.repositories;

import com.telerikproject.virtualcargarage.entities.Car;
import com.telerikproject.virtualcargarage.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    Car getById(long id);

    List<Car> getAllByCustomerId(long id);

    List<Car> getAllByCustomer_Email(String email);

    @Modifying
    @Transactional
    @Query(value="delete from Car c where c.id = :id")
    void deleteCarById(long id);

    boolean existsById(long id);

}
