package com.telerikproject.virtualcargarage.repositories;

import com.telerikproject.virtualcargarage.entities.CarService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface CarServiceRepository extends JpaRepository<CarService, Long> {

    CarService getById(long id);

    @Modifying
    @Transactional
    @Query(value="delete from CarService c where c.id = :id")
    void deleteCarServiceById(long id);
}
