package com.telerikproject.virtualcargarage.repositories;

import com.telerikproject.virtualcargarage.entities.Car;
import com.telerikproject.virtualcargarage.entities.Customer;
import com.telerikproject.virtualcargarage.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

//    @Query(nativeQuery = true, value = "SELECT * FROM car_service.customers WHERE is_deleted = false")
//    List<Customer> getAllCustomers();

    Customer getById(long id);

    Customer getByEmail(String email);

    Boolean existsCustomerByEmail(String email);

    @Query(value = "select c from Customer c where c.email not like ?1")
    Page<Customer> findAll(Pageable pageable, String username);

    Customer findByEmailIgnoreCase(String email);
}
