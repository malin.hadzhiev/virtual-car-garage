package com.telerikproject.virtualcargarage.repositories;

import com.telerikproject.virtualcargarage.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
    User findByUsernameIgnoreCase(String username);
}
