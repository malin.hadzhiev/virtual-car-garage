package com.telerikproject.virtualcargarage.repositories;

import com.telerikproject.virtualcargarage.entities.Car;
import com.telerikproject.virtualcargarage.entities.Visit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface VisitRepository extends JpaRepository<Visit, Long> {

    Visit getById(long id);

    List<Visit> getAllByCar_Customer_Email(String email);

    @Modifying
    @Transactional
    @Query(value="delete from Visit c where c.car.id = :carId")
    void deleteAllVisitsByCarId(long carId);

    @Modifying
    @Transactional
    @Query(value="delete from Visit v where v.id = :id")
    void deleteVisitById(long id);

}
