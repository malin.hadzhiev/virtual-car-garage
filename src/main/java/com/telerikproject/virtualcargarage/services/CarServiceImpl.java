package com.telerikproject.virtualcargarage.services;

import com.telerikproject.virtualcargarage.entities.Car;
import com.telerikproject.virtualcargarage.exceptions.DatabaseItemNotFoundException;
import com.telerikproject.virtualcargarage.repositories.CarRepository;
import com.telerikproject.virtualcargarage.repositories.VisitRepository;
import com.telerikproject.virtualcargarage.services.contracts.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    public static final String CARS_EXIST_BY_CUSTOMER_EMAIL = "No cars found for user with username %s.";
    public static final String CAR_EXIST_BY_ID = "Car with id %d not found.";
    public static final String CAR_LIST_IS_EMPTY = "Car list is empty!";

    private CarRepository carRepository;
    private VisitRepository visitRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository, VisitRepository visitRepository) {
        this.carRepository = carRepository;
        this.visitRepository = visitRepository;
    }

    @Override
    public List<Car> getAllCars() {
        List<Car> cars = carRepository.findAll();

        if (cars.isEmpty()) {
            throw new DatabaseItemNotFoundException(CAR_LIST_IS_EMPTY);
        }

        return cars;
    }

    @Override
    public Car getCarById(long id) {
        Car car = carRepository.getById(id);

        if (car == null) {
            throw new DatabaseItemNotFoundException(String.format(CAR_EXIST_BY_ID, id));
        }

        return car;
    }

    @Override
    public List<Car> getAllCarsByCustomerEmail(String email) {
        List<Car> cars = carRepository.getAllByCustomer_Email(email);

        if (cars == null) {
            throw new DatabaseItemNotFoundException(String.format(CARS_EXIST_BY_CUSTOMER_EMAIL, email));
        }

        return cars;
    }

    @Override
    public void createCar(Car car) {
        carRepository.save(car);
    }

    @Override
    public Car editCar(long id, Car car) {
        Car newCar = carRepository.getById(id);

        if (newCar == null) {
            throw new DatabaseItemNotFoundException(String.format(CAR_EXIST_BY_ID, id));
        }

        newCar.setRegistration(car.getRegistration());
        newCar.setCarVin(car.getCarVin());
        newCar.setManufacturer(car.getManufacturer());
        newCar.setModel(car.getModel());
        newCar.setYear(car.getYear());
        newCar.setCustomer(car.getCustomer());

        return carRepository.save(newCar);
    }

    @Transactional
    @Override
    public void deleteCar(long id) {
        Car car = carRepository.getById(id);

        if (car == null) {
            throw new DatabaseItemNotFoundException(String.format(CAR_EXIST_BY_ID, id));
        }

        visitRepository.deleteAllVisitsByCarId(id);
        carRepository.deleteCarById(id);
    }
}
