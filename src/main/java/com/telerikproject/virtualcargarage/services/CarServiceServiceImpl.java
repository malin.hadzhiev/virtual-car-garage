package com.telerikproject.virtualcargarage.services;

import com.telerikproject.virtualcargarage.entities.CarService;
import com.telerikproject.virtualcargarage.exceptions.DatabaseItemNotFoundException;
import com.telerikproject.virtualcargarage.repositories.CarServiceRepository;
import com.telerikproject.virtualcargarage.services.contracts.CarServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class CarServiceServiceImpl implements CarServiceService {

    public static final String SERVICE_EXIST_BY_ID = "Service with id %d not found.";
    public static final String SERVICE_LIST_IS_EMPTY = "Service list is empty!";

    private CarServiceRepository carServiceRepository;

    @Autowired
    public CarServiceServiceImpl(CarServiceRepository carServiceRepository) {
        this.carServiceRepository = carServiceRepository;
    }

    @Override
    public List<CarService> getAllCarServices() {
        List<CarService> carServices = carServiceRepository.findAll();

        if (carServices.isEmpty()) {
            throw new DatabaseItemNotFoundException(SERVICE_LIST_IS_EMPTY);
        }

        return carServices;
    }

    @Override
    public CarService getCarServiceById(long id) {
        CarService carService = carServiceRepository.getById(id);

        if (carService == null) {
            throw new DatabaseItemNotFoundException(String.format(SERVICE_EXIST_BY_ID, id));
        }

        return carService;
    }


    @Override
    public void createCarService(CarService carService) {
        carServiceRepository.save(carService);
    }

    @Override
    public void editCarService(long id, CarService carService) {
        CarService serviceToUpdate = carServiceRepository.getById(id);

        if (serviceToUpdate == null) {
            throw new DatabaseItemNotFoundException(String.format(SERVICE_EXIST_BY_ID, id));
        }

        serviceToUpdate.setServiceType(carService.getServiceType());
        serviceToUpdate.setPrice(carService.getPrice());

        carServiceRepository.save(serviceToUpdate);
    }

    @Override
    public void deleteService(long id) {
        CarService carService = carServiceRepository.getById(id);

        if (carService == null) {
            throw new DatabaseItemNotFoundException(String.format(SERVICE_EXIST_BY_ID, id));
        }

        carServiceRepository.deleteCarServiceById(id);
    }
}
