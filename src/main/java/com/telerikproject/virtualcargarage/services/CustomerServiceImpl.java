package com.telerikproject.virtualcargarage.services;

import com.telerikproject.virtualcargarage.entities.Car;
import com.telerikproject.virtualcargarage.entities.ConfirmationToken;
import com.telerikproject.virtualcargarage.entities.Customer;
import com.telerikproject.virtualcargarage.entities.User;
import com.telerikproject.virtualcargarage.exceptions.DatabaseItemNotFoundException;
import com.telerikproject.virtualcargarage.exceptions.DuplicateDatabaseItemFoundException;
import com.telerikproject.virtualcargarage.models.CustomerModel;
import com.telerikproject.virtualcargarage.models.CreateCustomerModel;
import com.telerikproject.virtualcargarage.repositories.CarRepository;
import com.telerikproject.virtualcargarage.repositories.ConfirmationTokenRepository;
import com.telerikproject.virtualcargarage.repositories.CustomerRepository;
import com.telerikproject.virtualcargarage.repositories.UserRepository;
import com.telerikproject.virtualcargarage.services.contracts.CarService;
import com.telerikproject.virtualcargarage.services.contracts.CustomerService;
import com.telerikproject.virtualcargarage.services.contracts.EmailService;
import com.telerikproject.virtualcargarage.services.contracts.PasswordService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.beans.FeatureDescriptor;
import java.util.List;
import java.util.stream.Stream;

@Service
public class CustomerServiceImpl implements CustomerService {

    public static final String CUSTOMER_EXIST_EXCEPTION = "Customer already exist!";
    public static final String AUTHORITY_ROLE = "ROLE_USER";
    private static final String REGISTRATION_EMAIL_SUBJECT = "Welcome to my CAR SERVICE!";
    public static final String CUSTOMER_EXIST_BY_EMAIL = "User with email %s not found!";
    public static final String CUSTOMER_EXIST_BY_ID = "Customer with id %d not found.";
    public static final String CUSTOMER_LIST_IS_EMPTY = "Customer list is empty!";
    public static final String INVALID_TOKEN = "Invalid token";

    private CustomerRepository customerRepository;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private PasswordService passwordService;
    private SimpleMailMessage simpleMailMessage;
    private EmailService emailService;
    private ConfirmationTokenRepository confirmationTokenRepository;
    private UserRepository userRepository;
    private CarRepository carRepository;
    private CarService carService;
    private ModelMapper mapper;

    @Autowired
    public CustomerServiceImpl(
            CustomerRepository customerRepository,
            UserDetailsManager userDetailsManager,
            PasswordEncoder passwordEncoder,
            PasswordService passwordService,
            SimpleMailMessage simpleMailMessage,
            EmailService emailService,
            ConfirmationTokenRepository confirmationTokenRepository,
            UserRepository userRepository,
            CarRepository carRepository,
            CarService carService,
            ModelMapper mapper) {

        this.customerRepository = customerRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.passwordService = passwordService;
        this.simpleMailMessage = simpleMailMessage;
        this.emailService = emailService;
        this.confirmationTokenRepository = confirmationTokenRepository;
        this.userRepository = userRepository;
        this.carRepository = carRepository;
        this.carService = carService;
        this.mapper = mapper;
    }

    @Override
    public List<Customer> getAllCustomers() {
        List<Customer> customers = customerRepository.findAll();

        if (customers.isEmpty()) {
            throw new DatabaseItemNotFoundException(CUSTOMER_LIST_IS_EMPTY);
        }

        return customers;
    }

    @Override
    public Customer getCustomerById(long id) {
        Customer customer = customerRepository.getById(id);

        if (customer == null) {
            throw new DatabaseItemNotFoundException(String.format(CUSTOMER_EXIST_BY_ID, id));
        }

        return customer;
    }

    @Override
    public Customer getCustomerByEmail(String email) {
        Customer customers = customerRepository.getByEmail(email);

        if (customers == null) {
            throw new DatabaseItemNotFoundException(String.format(
                    CUSTOMER_EXIST_BY_EMAIL, email));
        }

        return customers;
    }

    @Override
    @Transactional
    public void createCustomer(CreateCustomerModel createCustomerModel) {

        if (customerRepository.existsCustomerByEmail(createCustomerModel.getUsername())) {
            throw new DuplicateDatabaseItemFoundException(CUSTOMER_EXIST_EXCEPTION);
        }

        String password = passwordService.generateRandomPassword();
        String passwordEncoded = passwordEncoder.encode(password);

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(AUTHORITY_ROLE);
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        createCustomerModel.getUsername(),
                        passwordEncoded,
                        false,
                        true,
                        true,
                        true,
                        authorities);

        userDetailsManager.createUser(newUser);

        Customer customer = new Customer();
        customer.setEmail(createCustomerModel.getUsername());
        customer.setName(createCustomerModel.getName());
        customer.setPhone(createCustomerModel.getPhone());
        customer.setImagePath("default-user.png");

        ConfirmationToken confirmationToken = new ConfirmationToken(customer);
        confirmationTokenRepository.save(confirmationToken);

        String confirmationMessage = "To confirm your account, please click here : "
                + "http://localhost:8080/confirm-account?token=" + confirmationToken.getConfirmationToken();

        String message = simpleMailMessage.getText();

        String text = "";
        if (message != null) {
            text = String.format(message + confirmationMessage, newUser.getUsername(), password);
        }

        emailService.sendSimpleMessage(newUser.getUsername(), REGISTRATION_EMAIL_SUBJECT, text);
        customerRepository.save(customer);
    }

    @Override
    public void confirmAccount(String confirmationToken) {
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

        if (token == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, INVALID_TOKEN);
        }

        com.telerikproject.virtualcargarage.entities.User user =
                userRepository.findByUsernameIgnoreCase(token.getCustomer().getEmail());
        user.setEnabled(true);
        userRepository.save(user);
        confirmationTokenRepository.delete(token);
    }

    @Override
    public Customer editCustomer(long id, Customer customer) {
        Customer newCustomer = getCustomerById(id);

        if (newCustomer == null) {
            throw new DatabaseItemNotFoundException(String.format(CUSTOMER_EXIST_BY_ID, id));
        }

        newCustomer.setName(customer.getName());
        newCustomer.setPhone(customer.getPhone());

        return customerRepository.save(newCustomer);
    }

    @Override
    public void deleteCustomer(long id) {
        Customer customer = getCustomerById(id);

        List<Car> cars = carRepository.getAllByCustomerId(customer.getId());

        for (Car car : cars) {
            carService.deleteCar(car.getId());
        }

        userDetailsManager.deleteUser(customer.getEmail());
        customerRepository.delete(customer);
    }

    @Override
    public void save(Customer customer) {
        customerRepository.save(customer);
    }

//    @Override
//    public Page<Customer> customerPagination(Pageable pageable, String email) {
//        Customer customer = getCustomerByEmail(email);
//        return customerRepository.findAll(pageable, email);
//    }

    @Override
    public CustomerModel editCustomer(CustomerModel customerModel) {
        Customer customer = customerRepository.findById(customerModel.getId()).orElse(null);
        copyNonNullProperties(convertToEntity(customerModel), customer);
        return convertDto(customerRepository.save(customer));
    }

    private static void copyNonNullProperties(Object src, Object target) {
        BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
    }

    private static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        return Stream.of(src.getPropertyDescriptors())
                .map(FeatureDescriptor::getName)
                .filter(propertyName -> src.getPropertyValue(propertyName) == null || src.getPropertyValue(propertyName).equals(""))
                .toArray(String[]::new);
    }

    private Customer convertToEntity(CustomerModel customerModel) {
        return mapper.map(customerModel, Customer.class);
    }

    private CustomerModel convertDto(Customer customer) {
        return mapper.map(customer, CustomerModel.class);
    }

}
