package com.telerikproject.virtualcargarage.services;

import com.telerikproject.virtualcargarage.entities.User;
import com.telerikproject.virtualcargarage.exceptions.WrongPasswordException;
import com.telerikproject.virtualcargarage.repositories.UserRepository;
import com.telerikproject.virtualcargarage.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.web.server.ResponseStatusException;

@Service
public class UserServiceImpl implements UserService {

    public static final String WRONG_PASSWORD = "Wrong password!";
    public static final String CUSTOMER_EXIST = "Customer doesn't exist";

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.findByUsernameIgnoreCase(email);
    }

    @Transactional
    @Override
    public void changePassword(com.telerikproject.virtualcargarage.entities.User user, String currentPassword, String newPassword) {
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, CUSTOMER_EXIST);
        }

        if (!passwordEncoder.matches(currentPassword, user.getPassword())) {
            throw new WrongPasswordException(WRONG_PASSWORD);
        }

        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(user);
    }
}