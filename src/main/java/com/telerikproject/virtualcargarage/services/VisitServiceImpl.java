package com.telerikproject.virtualcargarage.services;

import com.telerikproject.virtualcargarage.entities.CarService;
import com.telerikproject.virtualcargarage.entities.Visit;
import com.telerikproject.virtualcargarage.exceptions.DatabaseItemNotFoundException;
import com.telerikproject.virtualcargarage.models.CreateVisitModel;
import com.telerikproject.virtualcargarage.repositories.CarRepository;
import com.telerikproject.virtualcargarage.repositories.CarServiceRepository;
import com.telerikproject.virtualcargarage.repositories.VisitRepository;
import com.telerikproject.virtualcargarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class VisitServiceImpl implements VisitService {

    public static final String VISIT_LIST_IS_EMPTY = "Visit list is empty!";
    public static final String VISIT_EXIST_BY_ID = "Car with id %d not found";
    public static final String VISIT_EXIST_BY_CUSTOMER_EMAIL = "No visits found for user with username %s";

    private VisitRepository visitRepository;
    private CarServiceRepository carServiceRepository;
    private CarRepository carRepository;

    @Autowired
    public VisitServiceImpl(VisitRepository visitRepository, CarServiceRepository carServiceRepository, CarRepository carRepository) {
        this.visitRepository = visitRepository;
        this.carServiceRepository = carServiceRepository;
        this.carRepository = carRepository;
    }

    @Override
    public List<Visit> getAllVisits() {
        List<Visit> visits = visitRepository.findAll();

        if (visits.isEmpty()) {
            throw new DatabaseItemNotFoundException(VISIT_LIST_IS_EMPTY);
        }

        return visits;
    }

    @Override
    public Visit getVisitById(long id) {
        Visit visit = visitRepository.getById(id);

        if (visit == null) {
            throw new DatabaseItemNotFoundException(String.format(VISIT_EXIST_BY_ID, id));
        }

        return visit;
    }

    @Override
    public List<Visit> getAllVisitsByCustomerEmail(String email) {
        List<Visit> visits = visitRepository.getAllByCar_Customer_Email(email);

        if (visits == null) {
            throw new DatabaseItemNotFoundException(String.format(VISIT_EXIST_BY_CUSTOMER_EMAIL, email));
        }

        return visits;
    }

    @Override
    @Transactional
    public void editVisit(long id, CreateVisitModel createVisitModel) {
        Visit newVisit = getVisitById(id);

        if (newVisit == null) {
            throw new DatabaseItemNotFoundException(String.format(VISIT_EXIST_BY_ID, id));
        }

        Set<CarService> carServices = new HashSet<>();
        double totalPrice = 0;


        for (int i = 0; i < createVisitModel.getService().size(); i++) {
            CarService carService = carServiceRepository.getById(Integer.parseInt(createVisitModel.getService().get(i)));
            carServices.add(carService);
            totalPrice += carService.getPrice();
        }

        newVisit.setCar(carRepository.getById(Long.parseLong(createVisitModel.getRegistration())));
        newVisit.setDate(LocalDateTime.now());
        newVisit.setTotalPrice(totalPrice);
        newVisit.setCarServices(carServices);

        visitRepository.save(newVisit);
    }

    @Override
    @Transactional
    public void createVisit(String registration, List<String> services) {
        Set<CarService> carServices = new HashSet<>();
        double totalPrice = 0;

        for (String service : services) {
            CarService carService = carServiceRepository.getById(Integer.parseInt(service));
            carServices.add(carService);
            totalPrice += carService.getPrice();
        }

        Visit visit = new Visit();

        visit.setTotalPrice(totalPrice);
        visit.setCar(carRepository.getById(Long.parseLong(registration)));
        visit.setDate(LocalDateTime.now());
        visit.setCarServices(carServices);

        visitRepository.save(visit);
    }

    @Override
    public void deleteVisit(long id) {
        Visit visit = visitRepository.getById(id);

        if (visit == null) {
            throw new DatabaseItemNotFoundException(String.format(VISIT_EXIST_BY_ID, id));
        }

        visitRepository.deleteVisitById(id);
    }
}
