package com.telerikproject.virtualcargarage.services.contracts;

import com.telerikproject.virtualcargarage.entities.Car;

import java.util.List;

public interface CarService {

    List<Car> getAllCars();

    Car getCarById(long id);

    List<Car> getAllCarsByCustomerEmail(String email);

    void createCar(Car car);

    void deleteCar(long id);

    Car editCar(long id, Car car);
}
