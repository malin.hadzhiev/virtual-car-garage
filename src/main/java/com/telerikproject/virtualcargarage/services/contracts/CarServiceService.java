package com.telerikproject.virtualcargarage.services.contracts;

import com.telerikproject.virtualcargarage.entities.CarService;

import java.util.List;

public interface CarServiceService {
    List<CarService> getAllCarServices();

    CarService getCarServiceById(long id);

    void createCarService(CarService carService);

    void editCarService(long id, CarService carService);

    void deleteService(long id);
}
