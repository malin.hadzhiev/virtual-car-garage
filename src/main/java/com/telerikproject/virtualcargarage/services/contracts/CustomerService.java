package com.telerikproject.virtualcargarage.services.contracts;

import com.telerikproject.virtualcargarage.entities.Customer;
import com.telerikproject.virtualcargarage.models.CustomerModel;
import com.telerikproject.virtualcargarage.models.EditCustomerModel;
import com.telerikproject.virtualcargarage.models.CreateCustomerModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomers();

    Customer getCustomerById(long id);

    Customer getCustomerByEmail(String email);

    @Transactional
    void createCustomer(CreateCustomerModel createUserModel);


    void confirmAccount(String confirmationToken);

//    @Transactional
//    void changePassword(EditCustomerModel editCustomerModel);

    Customer editCustomer(long id, Customer customer);

    void deleteCustomer(long id);

    void save(Customer customer);

    CustomerModel editCustomer(CustomerModel customerModel);

//    Page<Customer> customerPagination(Pageable pageable, String email);
}
