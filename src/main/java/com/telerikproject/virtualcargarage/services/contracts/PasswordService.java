package com.telerikproject.virtualcargarage.services.contracts;

public interface PasswordService {
    String generateRandomPassword();

}
