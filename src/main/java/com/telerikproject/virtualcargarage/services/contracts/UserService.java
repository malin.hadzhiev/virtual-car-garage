package com.telerikproject.virtualcargarage.services.contracts;

import com.telerikproject.virtualcargarage.entities.User;
import org.springframework.transaction.annotation.Transactional;

public interface UserService {
    User getUserByEmail(String email);

    @Transactional
    void changePassword(User user, String oldPassword, String newPassword);
}
