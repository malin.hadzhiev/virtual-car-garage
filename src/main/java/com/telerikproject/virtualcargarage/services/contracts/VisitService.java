package com.telerikproject.virtualcargarage.services.contracts;

import com.telerikproject.virtualcargarage.entities.CarService;
import com.telerikproject.virtualcargarage.entities.Visit;
import com.telerikproject.virtualcargarage.models.CreateVisitModel;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

public interface VisitService {
    List<Visit> getAllVisits();

    Visit getVisitById(long id);

    List<Visit> getAllVisitsByCustomerEmail(String email);

//    void editVisit(long id, Visit visit);

    @Transactional
    void editVisit(long id, CreateVisitModel createVisitModel);

    void createVisit(String registration, List<String> services);

    void deleteVisit(long id);
}
