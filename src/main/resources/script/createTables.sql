use car_service;
create table customers
(
    id         bigint auto_increment
        primary key,
    email      varchar(50)  not null,
    name       varchar(20)  null,
    phone      varchar(15)  null,
    image_path varchar(255) null,
    constraint customers_email_uindex
        unique (email)
);

create table cars
(
    id            bigint auto_increment
        primary key,
    customer_id   bigint      null,
    license_plate varchar(50) not null,
    car_vin       varchar(50) not null,
    manufacturer  varchar(20) null,
    model         varchar(20) null,
    year          int         null,
    constraint customer_cars_car_vin_uindex
        unique (car_vin),
    constraint customer_cars_license_plate_uindex
        unique (license_plate),
    constraint customer_cars_customers_id_fk
        foreign key (customer_id) references customers (id)
);

create table confirmation_token
(
    id                 bigint auto_increment
        primary key,
    confirmation_token varchar(255) null,
    created_date       datetime     null,
    customer_id        bigint       null,
    constraint confirmation_token_customers_id_fk
        foreign key (customer_id) references customers (id)
);

create table services
(
    id           bigint auto_increment
        primary key,
    service_type varchar(50) null,
    price        double      null,
    visit_id     bigint      null
);

create index services_visits_id_fk
    on services (visit_id);

create table users
(
    username varchar(50) not null
        primary key,
    password varchar(68) not null,
    enabled  tinyint     not null
);

create table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint username_authority
        unique (username, authority),
    constraint FK__users
        foreign key (username) references users (username)
);

create table visits
(
    id          bigint auto_increment
        primary key,
    car_id      bigint   null,
    date        datetime null,
    total_price double   null,
    constraint visits_customer_cars_id_fk
        foreign key (car_id) references cars (id)
);

create table visit_services
(
    visit_id   bigint null,
    service_id bigint null,
    constraint visit_services_services_id_fk
        foreign key (service_id) references services (id),
    constraint visit_services_visits_id_fk
        foreign key (visit_id) references visits (id)
);

