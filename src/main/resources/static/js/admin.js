$(document).ready(function () {
    fillEditPopup();
});

function fillEditPopup() {

    let testVar = undefined;
    let $newOne = undefined;

    $("#table-content").on("click", ".edit-user-final", function () {
        let userId = $(this).closest("#table-content").find(".user-id-current").attr("id");

        testVar = userId;

        console.log(testVar);

        fetch("http://localhost:8080/api/customers/" + testVar, {
            method: "GET",
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json, text/plain, */*"
            }
        })
            .then((res) => res.json())
            .then(function (ele) {
                $("#name-edit").val(ele.name);
                $("#phone-edit").val(ele.phone);
            })
            .catch((err) => {
                console.log(err.message);
            })
    });

    $(".submit-edit").on("click", function () {
        let obj = {
            "id": testVar,
            "name": $("#name-edit").val(),
            "phone": $("#phone-edit").val(),
        };

        console.log(obj);

        fetch("http://localhost:8080/api/customers/edit", {
            method: "PUT",
            body: JSON.stringify(obj),
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json, text/plain, */*"
            }
        })
            .then((res) => res.json())
            .then(function (el) {
                console.log(el);
                $($newOne).text(el.name);
            })
            .catch((err) => {
                console.log(err.message);
            })
    })
}