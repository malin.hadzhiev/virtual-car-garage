$(function () {
    getAndLoadContent(8, 0);
    nextPage();
    previousPage();
});

function previousPage() {
    $("#previous-page").on("click", function () {
        getAndLoadContent(8, parseInt($("#current-page-value").text()) - 2);
    })
}

function nextPage() {
    $("#next-page").on("click", function () {
        getAndLoadContent(8, parseInt($("#current-page-value").text()));
    })
}

function getAndLoadContent(size, page) {
    $.ajax({
        url: window.location.origin + "/api/customers/page/",
        type: "GET",
        data: {
            size: size,
            page: page,
        },
        success: function (response) {
            let $tableContent = $("#table-content");
            $tableContent.empty();
            response.content.forEach(function (el) {
                // $tableContent.append(
                // `<tr>
                //             <td align="center">
                //                  <a id="editCustomer${el.id}" href="/admin/edit/customer/${el.id}"
                //                    class="btn btn-default"><em class="fa fa-pencil"></em></a>
                //                 <a th:href="@{'/admin/customer/delete/' + ${customer.getId()}}"
                //                    onclick="return confirm('Are you sure you want to delete this customer?');"
                //                    class="btn btn-danger"><em class="fa fa-trash"></em></a>
                //
                //
                //             </td>
                //             <td th:text="${customer.getId()}" class="hidden-xs">1</td>
                //             <td th:text="${customer.getName()}">John Doe</td>
                //             <td th:text="${customer.getEmail()}">johndoe@example.com</td>
                //             <td th:text="${customer.getPhone()}"></td>
                //             <td th:text="${customer.getCars()}"></td>
                //         </tr>`
                //     )

                $tableContent.append(
                    "<div class='col-3 some-content-here user-content'>" +
                    "<figure class='rectangle-form'>" +
                    "   <img src= 'images/" + el.picture + "' alt=\"\">" +
                    "<figcaption cla>" +
                    "   <div id='"+ el.id +"' style='font-size: smaller' class='users-list-name'>"+ el.firstName + " " + el.lastName +"</div>" +
                    "   <div class='row'>" +
                    "       <a class=\"button edit-user-final btn btn-primary\" href=\"#popup1\">Edit</a>" +
                    "       <div class='col-6'>" +
                    "           <button type='button' class='btn btn-primary btn-xs delete-user'>Del</button>" +
                    "       </div>" +
                    "   </div>" +
                    "</figcaption>" +
                    "</figure>" +
                    "</div>"
                )
            });
            $("#current-page-value").text(response.pageable.pageNumber + 1);
            enableDisableButton($("#previous-page"), response.first);
            enableDisableButton($("#next-page"), response.last);
        },
        error: function (err) {
            console.log(err);
            console.log("error");
        }
    })
}

function enableDisableButton($button, shouldDisabled) {
    if (shouldDisabled) {
        $button.addClass("li-disabled");
    } else if ($button.hasClass("li-disabled")) {
        $button.removeClass("li-disabled");
    }
}