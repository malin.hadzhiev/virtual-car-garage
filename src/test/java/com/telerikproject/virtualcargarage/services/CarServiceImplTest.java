package com.telerikproject.virtualcargarage.services;

import com.telerikproject.virtualcargarage.entities.Car;
import com.telerikproject.virtualcargarage.entities.Customer;
import com.telerikproject.virtualcargarage.exceptions.DatabaseItemNotFoundException;
import com.telerikproject.virtualcargarage.repositories.CarRepository;


import com.telerikproject.virtualcargarage.repositories.VisitRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;


import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


import java.util.ArrayList;
import java.util.List;


import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class CarServiceImplTest {

    @Mock
    CarRepository carRepository;
    VisitRepository visitRepository;

    @InjectMocks
    private CarServiceImpl carService;
    private Car mockCarOne = new Car();
    private Car mockCarTwo = new Car();
    private Customer customer = new Customer();
    private List<Car> carsList = new ArrayList();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockCarOne.setId(1);
        mockCarOne.setCustomer(customer);
        mockCarOne.setRegistration("CA5858BN");
        mockCarOne.setCarVin("MATBADC456M1492SA");
        mockCarOne.setYear(2000);
        mockCarOne.setManufacturer("Ford");
        mockCarOne.setModel("Legend");

        mockCarTwo.setId(2);
        mockCarTwo.setCustomer(customer);
        mockCarTwo.setRegistration("CB9765PD");
        mockCarTwo.setCarVin("MATBAAA456M1492SA");
        mockCarTwo.setYear(2000);
        mockCarOne.setManufacturer("Tesla");
        mockCarTwo.setModel("Cougar");

        carsList.add(mockCarOne);
        carsList.add(mockCarTwo);
    }

    @Test
    public void getAllCars_ShouldReturn_WhenValidArgsPassed() {
        when(carRepository.findAll()).thenReturn(carsList);

        List<Car> result = carService.getAllCars();

        assertEquals(2, result.size());
    }

    @Test(expected = DatabaseItemNotFoundException.class)
    public void getAllCars_ShouldThrow_WhenNoCarsFound() {
        when(carRepository.findAll()).thenReturn(new ArrayList<>());

        List<Car> result = carService.getAllCars();

        assertEquals(1, result.size());
    }

    @Test
    public void getCarByID_ShouldReturn_WhenValidArgsPassed(){
        when(carRepository.getById(1L))
                .thenReturn(mockCarOne);
        Car result = carService.getCarById(1L);
        assertEquals(result, mockCarOne);
    }

    @Test(expected = DatabaseItemNotFoundException.class)
    public void getCarByID_ShouldThrow_WhenNoCarsFound() {
        when(carRepository.getById(1L))
                .thenReturn(null);
        Car result = carService.getCarById(1L);
        assertEquals(result, mockCarOne);
    }

    @Test
    public void getAllCarsByCustomerEmail_ShouldReturn_WhenValidArgsPassed(){
        when(carRepository.getAllByCustomer_Email(customer.getEmail()))
                .thenReturn(carsList);
        List<Car> result = carService.getAllCarsByCustomerEmail(customer.getEmail());
        assertEquals(2, result.size());
    }

//    @Test(expected = DatabaseItemNotFoundException.class)
//    public void getAllCarsByCustomerEmail_ShouldThrow_WhenNoCarsFound(){
//        when(carRepository.getAllByCustomer_Email(customer.getEmail()))
//                .thenReturn(new ArrayList<>());
//
//        List<Car> result = carService.getAllCarsByCustomerEmail(customer.getEmail());
//
//        assertEquals(0, result.size());
//    }

    @Test
    public void create_Should_CallRepository_When_Create_NewCar() {
        // Arrange
        Car car = new Car();

        // Act
        carService.createCar(car);

        // Assert
        Mockito.verify(carRepository, Mockito.times(1))
                .save(car);
    }

//    @Test
//    public void deleteService_ShouldDeleteService() {
//        when(carRepository.getById(1)).thenReturn(mockCarOne);
//
//        carService.deleteCar(1);
//
//        verify(visitRepository, Mockito.times(1)).deleteAllVisitsByCarId(1);
//        verify(carRepository, Mockito.times(1)).deleteCarById(1);
//    }

    @Test(expected = DatabaseItemNotFoundException.class)
    public void deleteCarByID_ShouldThrow_WhenNoCarFound(){
        carService.deleteCar(1);
        verify(carRepository, Mockito.times(1)).deleteCarById(1);
    }

    @Test
    public void editCar_ShouldEditCar() {
        when(carRepository.getById(1)).thenReturn(mockCarOne);

        carService.editCar(1, mockCarOne);

        verify(carRepository, Mockito.times(1)).save(any(Car.class));
    }
}
