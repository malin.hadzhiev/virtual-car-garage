package com.telerikproject.virtualcargarage.services;

import com.telerikproject.virtualcargarage.entities.Car;
import com.telerikproject.virtualcargarage.entities.CarService;
import com.telerikproject.virtualcargarage.entities.Customer;
import com.telerikproject.virtualcargarage.entities.Visit;
import com.telerikproject.virtualcargarage.exceptions.DatabaseItemNotFoundException;
import com.telerikproject.virtualcargarage.repositories.CarServiceRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CarServiceServiceImplTest {

    @Mock
    CarServiceRepository serviceRepository;

    @InjectMocks
    private CarServiceServiceImpl serviceService;
    private CarService mockServiceOne = new CarService();
    private CarService mockServiceTwo = new CarService();
    private List<Visit> visits = new ArrayList<>();
    private List<CarService> servicesList = new ArrayList();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockServiceOne.setCarVisits(visits);
        mockServiceOne.setServiceType("Oil change");
        mockServiceOne.setPrice(20.5);

        mockServiceTwo.setCarVisits(visits);
        mockServiceTwo.setServiceType("Oil change1");
        mockServiceTwo.setPrice(50.5);

        servicesList.add(mockServiceOne);
        servicesList.add(mockServiceTwo);
    }

    @Test
    public void getAllServices_ShouldReturn_WhenValidArgsPassed() {
        when(serviceRepository.findAll()).thenReturn(servicesList);

        List<CarService> result = serviceService.getAllCarServices();

        assertEquals(2, result.size());
    }

    @Test(expected = DatabaseItemNotFoundException.class)
    public void getAllServices_ShouldThrow_WhenNoServicesFound() {
        when(serviceRepository.findAll()).thenReturn(new ArrayList<>());

        List<CarService> result = serviceService.getAllCarServices();

        assertEquals(1, result.size());
    }

    @Test
    public void getServiceByID_ShouldReturn_WhenValidArgsPassed(){
        when(serviceRepository.getById(1L))
                .thenReturn(mockServiceOne);
        CarService result = serviceService.getCarServiceById(1L);
        assertEquals(result, mockServiceOne);
    }

    @Test(expected = DatabaseItemNotFoundException.class)
    public void getServiceByID_ShouldThrow_WhenNoServicesFound() {
        when(serviceRepository.getById(1L))
                .thenReturn(null);
        CarService result = serviceService.getCarServiceById(1L);
        assertEquals(result, mockServiceOne);
    }


    @Test
    public void create_Should_CallRepository_When_Create_NewService() {
        // Arrange
        CarService service = new CarService();

        // Act
        serviceService.createCarService(service);

        // Assert
        Mockito.verify(serviceRepository, Mockito.times(1))
                .save(service);
    }

    @Test(expected = DatabaseItemNotFoundException.class)
    public void deleteServiceByID_ShouldThrow_WhenNoServiceFound(){
        serviceService.deleteService(1);
        verify(serviceRepository, Mockito.times(1)).deleteCarServiceById(1);
    }

    @Test
    public void editService_ShouldEditService() {
        when(serviceRepository.getById(1)).thenReturn(mockServiceOne);

        serviceService.editCarService(1, mockServiceOne);

        verify(serviceRepository, Mockito.times(1)).save(any(CarService.class));
    }

    @Test
    public void deleteService_ShouldDeleteService() {
        when(serviceRepository.getById(1)).thenReturn(mockServiceOne);

        serviceService.deleteService(1);

        verify(serviceRepository, Mockito.times(1)).deleteCarServiceById(1);
    }
}
