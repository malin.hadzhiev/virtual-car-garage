package com.telerikproject.virtualcargarage.services;

import com.telerikproject.virtualcargarage.entities.Car;
import com.telerikproject.virtualcargarage.entities.ConfirmationToken;
import com.telerikproject.virtualcargarage.entities.Customer;
import com.telerikproject.virtualcargarage.entities.User;
import com.telerikproject.virtualcargarage.exceptions.DatabaseItemNotFoundException;
import com.telerikproject.virtualcargarage.models.CreateCustomerModel;
import com.telerikproject.virtualcargarage.repositories.CarRepository;
import com.telerikproject.virtualcargarage.repositories.ConfirmationTokenRepository;
import com.telerikproject.virtualcargarage.repositories.CustomerRepository;
import com.telerikproject.virtualcargarage.repositories.VisitRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CustomerServiceImplTest {

    @Mock
    CustomerRepository customerRepository;
    ConfirmationTokenRepository confirmationTokenRepository;
    PasswordEncoder passwordEncoder;
    UserDetailsManager userDetailsManager;
    CarRepository carRepository;

    @InjectMocks
    private CustomerServiceImpl customerService;
    private CarServiceImpl carService;
    private PasswordServiceImpl passwordService;
    private CreateCustomerModel createCustomerModel;
    private Customer mockCustomerOne = new Customer();
    private Customer mockCustomerTwo = new Customer();
    private List<Customer> customersList = new ArrayList();
    private List<Car> cars = new ArrayList();
    private User user = new User();
    private Car car = new Car();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockCustomerOne.setCars(cars);
        mockCustomerOne.setEmail("malin@mailinator.com");
        mockCustomerOne.setName("Malin");
        mockCustomerOne.setPhone("0896541347");

        mockCustomerOne.setCars(cars);
        mockCustomerOne.setEmail("canko@mailinator.com");
        mockCustomerOne.setName("Tsanko");
        mockCustomerOne.setPhone("0896541347");

        car.setId(1);
        car.setCustomer(mockCustomerOne);
        car.setRegistration("CA5858BN");
        car.setCarVin("MATBADC456M1492SA");
        car.setYear(2000);
        car.setManufacturer("Ford");
        car.setModel("Legend");

        customersList.add(mockCustomerOne);
        customersList.add(mockCustomerTwo);
        cars.add(car);
    }

    @Test
    public void getAllCustomers_ShouldReturn_WhenValidArgsPassed() {
        when(customerRepository.findAll()).thenReturn(customersList);

        List<Customer> result = customerService.getAllCustomers();

        assertEquals(2, result.size());
}


    @Test(expected = DatabaseItemNotFoundException.class)
    public void getAllCustomers_ShouldThrow_WhenNoCustomersFound() {
        when(customerRepository.findAll()).thenReturn(new ArrayList<>());

        List<Customer> result = customerService.getAllCustomers();

        assertEquals(1, result.size());
    }

    @Test
    public void getCustomerByID_ShouldReturn_WhenValidArgsPassed(){
        when(customerRepository.getById(1L))
                .thenReturn(mockCustomerOne);
        Customer result = customerService.getCustomerById(1L);
        assertEquals(result, mockCustomerOne);
    }

    @Test(expected = DatabaseItemNotFoundException.class)
    public void getCustomerByID_ShouldThrow_WhenNoCustomersFound() {
        when(customerRepository.getById(1L))
                .thenReturn(null);
        Customer result = customerService.getCustomerById(1L);
        assertEquals(result, mockCustomerOne);
    }

    @Test
    public void getCustomerByEmail_ShouldReturn_WhenValidArgsPassed(){
        when(customerRepository.getByEmail(mockCustomerOne.getEmail()))
                .thenReturn(mockCustomerOne);
        Customer result = customerService.getCustomerByEmail(mockCustomerOne.getEmail());
        assertEquals(result, mockCustomerOne);
    }

    @Test(expected = DatabaseItemNotFoundException.class)
    public void getCustomerByEmail_ShouldThrow_WhenNoCustomersFound() {
        when(customerRepository.getByEmail(mockCustomerOne.getEmail()))
                .thenReturn(null);
        Customer result = customerService.getCustomerByEmail(mockCustomerOne.getEmail());
        assertEquals(result, mockCustomerOne);
    }

    @Test
    public void save_Should_CallRepositorySave_When_SavingCustomer() {
        // Arrange
        Customer customer = new Customer();

        // Act
        customerService.save(customer);

        // Assert
        Mockito.verify(customerRepository, Mockito.times(1)).save(customer);
    }

    @Test
    public void editCustomer_ShouldEditCustomer() {
        when(customerRepository.getById(1)).thenReturn(mockCustomerOne);

        customerService.editCustomer(1, mockCustomerOne);

        verify(customerRepository, Mockito.times(1)).save(any(Customer.class));
    }

//    @Test
//    public void deleteCustomer_ShouldDeleteCustomer() {
//        when(customerRepository.getById(1)).thenReturn(mockCustomerOne);
//        when(carRepository.getAllByCustomerId(1)).thenReturn(cars);
//
//        customerService.deleteCustomer(1);
//
//        verify(carService, Mockito.times(1)).deleteCar(1L);
//        verify(userDetailsManager, Mockito.times(1)).deleteUser(mockCustomerOne.getEmail());
//        verify(customerRepository, Mockito.times(1)).deleteById(1L);
//    }
}
