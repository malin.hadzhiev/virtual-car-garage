package com.telerikproject.virtualcargarage.services;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.mockito.ArgumentMatchers.any;

import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class EmailServiceImplTest {

    @Mock
    JavaMailSender mailSender;

    @InjectMocks
    EmailServiceImpl emailService;

    @Test
    public void sendSimpleMessage_Should_SendEmailMessage() {

        emailService.sendSimpleMessage("email@email.com", "subject", "text");

        Mockito.verify(mailSender, times(1)).send(any(SimpleMailMessage.class));
    }
}
