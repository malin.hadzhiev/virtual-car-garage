package com.telerikproject.virtualcargarage.services;


import com.telerikproject.virtualcargarage.services.contracts.PasswordService;
import org.junit.Test;

import static org.junit.Assert.*;

public class PasswordServiceImplTest {

    private PasswordService passwordService = new PasswordServiceImpl();

    @Test
    public void whenPasswordGeneratedUsingPassay_thenSuccessful() {
        String password = passwordService.generateRandomPassword();
        int specialCharCount = 0;
        for (char c : password.toCharArray()) {
                specialCharCount++;
        }
        assertTrue("Password validation failed in Passay", specialCharCount >= 2);
    }
}