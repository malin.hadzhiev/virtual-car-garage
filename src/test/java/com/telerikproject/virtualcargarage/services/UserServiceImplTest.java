package com.telerikproject.virtualcargarage.services;

import com.telerikproject.virtualcargarage.entities.User;
import com.telerikproject.virtualcargarage.exceptions.WrongPasswordException;
import com.telerikproject.virtualcargarage.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class UserServiceImplTest {

    @Mock
    UserRepository userRepository;
    PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserServiceImpl userService;
    private User mockUser = new User();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockUser.setUsername("malin@mailinator.com");
        mockUser.setPassword(passwordEncoder.encode("dfghjfd"));
        mockUser.setEnabled(true);

    }

//    @Test
//    public void getUserByEmail_ShouldReturn_WhenValidArgsPassed(){
//        when(userRepository.findByUsernameIgnoreCase(mockUser.getUsername()))
//                .thenReturn(mockUser);
//        User result = userService.getUserByEmail(mockUser.getUsername());
//        assertEquals(result, mockUser);
//    }
//
//    @Test
//    public void givenPasswordToChange_whenChangePasswordCalled_thenSuccess(){
//        when(userRepository.save(any(User.class))).thenReturn(mockUser);
//        Mockito.when(passwordEncoder.encode(any(String.class))).thenReturn("currentPassword");
//
//        userService.changePassword(mockUser, "currentPassword", "newPassword");
//
//        Mockito.verify(userRepository).save(mockUser);
//    }
//
//    @Test(expected = ResponseStatusException.class)
//    public void givenPasswordToChange_whenChangePasswordCalled_thenUserNotFound(){
//        userService.changePassword(new User(), "currentPassword", "newPassword");
//    }
//
//    @Test(expected = WrongPasswordException.class)
//    public void givenPasswordToChange_whenChangePasswordCalled_thenPasswordsNotMatch(){
//        userService.changePassword(mockUser, "currentPassword", "newPassword");
//    }
}
