package com.telerikproject.virtualcargarage.services;

import com.telerikproject.virtualcargarage.entities.Car;
import com.telerikproject.virtualcargarage.entities.CarService;
import com.telerikproject.virtualcargarage.entities.Customer;
import com.telerikproject.virtualcargarage.entities.Visit;
import com.telerikproject.virtualcargarage.exceptions.DatabaseItemNotFoundException;
import com.telerikproject.virtualcargarage.models.CreateVisitModel;
import com.telerikproject.virtualcargarage.repositories.VisitRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class VisitServiceImplTest {

    @Mock
    VisitRepository visitRepository;

    @InjectMocks
    private VisitServiceImpl visitService;
    private Visit mockVisitOne = new Visit();
    private Visit mockVisitTwo = new Visit();
    private Car car = new Car();
    private Set<CarService> services = new HashSet<>();
    private List<Visit> visitsList = new ArrayList();
    private Customer customer = new Customer();
    private CreateVisitModel createVisitModel = new CreateVisitModel();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockVisitOne.setCar(car);
        mockVisitOne.setCarServices(services);
        mockVisitOne.setDate(LocalDateTime.now());
        mockVisitOne.setTotalPrice(150.2);

        mockVisitOne.setCar(car);
        mockVisitOne.setCarServices(services);
        mockVisitOne.setDate(LocalDateTime.now());
        mockVisitOne.setTotalPrice(110.2);

        visitsList.add(mockVisitOne);
        visitsList.add(mockVisitTwo);
    }

    @Test
    public void getAllVisits_ShouldReturn_WhenValidArgsPassed() {
        when(visitRepository.findAll()).thenReturn(visitsList);

        List<Visit> result = visitService.getAllVisits();

        assertEquals(2, result.size());
    }

    @Test(expected = DatabaseItemNotFoundException.class)
    public void getAllVisits_ShouldThrow_WhenNoVisitsFound() {
        when(visitRepository.findAll()).thenReturn(new ArrayList<>());

        List<Visit> result = visitService.getAllVisits();

        assertEquals(1, result.size());
    }

    @Test
    public void getVisitByID_ShouldReturn_WhenValidArgsPassed(){
        when(visitRepository.getById(1L))
                .thenReturn(mockVisitOne);
        Visit result = visitService.getVisitById(1L);
        assertEquals(result, mockVisitOne);
    }

    @Test(expected = DatabaseItemNotFoundException.class)
    public void getVisitByID_ShouldThrow_WhenNoVisitsFound() {
        when(visitRepository.getById(1L))
                .thenReturn(null);
        Visit result = visitService.getVisitById(1L);
        assertEquals(result, mockVisitOne);
    }

    @Test
    public void getAllVisitsByCustomerEmail_ShouldReturn_WhenValidArgsPassed(){
        when(visitRepository.getAllByCar_Customer_Email(customer.getEmail()))
                .thenReturn(visitsList);
        List<Visit> result = visitService.getAllVisitsByCustomerEmail(customer.getEmail());
        assertEquals(2, result.size());
    }

    @Test
    public void deleteService_ShouldDeleteService() {
        when(visitRepository.getById(1)).thenReturn(mockVisitOne);

        visitService.deleteVisit(1);

        verify(visitRepository, Mockito.times(1)).deleteVisitById(1);
    }

    @Test(expected = DatabaseItemNotFoundException.class)
    public void deleteVisitByID_ShouldThrow_WhenNoVisitFound(){
        Mockito.when(visitRepository.findById(1L)).thenReturn(java.util.Optional.ofNullable(mockVisitOne));

        visitService.deleteVisit(1);

        Mockito.verify(visitRepository, Mockito.times(1)).delete(mockVisitOne);
    }

//    @Test
//    public void create_Should_CallRepository_When_Create_NewVisit() {
//        // Arrange
//        String registration = car.getRegistration();
//        List<String> carServices = new ArrayList<>();
//        Visit visit = new Visit();
//        // Act
//        visitService.createVisit(registration, carServices);
//
//        // Assert
//        Mockito.verify(visitRepository, Mockito.times(1))
//                .save(visit);
//    }
}
